var express = require('express');
var router = express.Router();
var https = require('https');
var debug = require('debug')('girl:index.js');
var RND = require('random-js');
var random = new RND(RND.engines.mt19937().autoSeed());
var request = require('request');
var fs = require('fs');
var NodeCache = require('node-cache');
var groupCache = new NodeCache({stdTTL: 60, checkPeriod: 120});
/* GET home page. */
var roll = require('./roll.js');
var new_roll = require('./new_roll.js').new_roll;
router.get('/', function(req, res) {
  res.render('ng/index');
});
router.get('/ng', (req, res) => {
   res.render('ng/index');
});
router.post('/random', (req, res) => {
  roll(req, res)
});
router.get('/rnd', (req, res) => {
    new_roll(req, res);
    //res.send('<div>Error</div>');
});
router.get('/photo_proxy/', (req, res) => {
    var link = req.query.link || '';
    debug('[LINK]: ', link);
    if (link && (link.match(/http:\/\/cs\d*\.vk\.me[\/\w+\/\-_]+\.(jpg|png)$/) || link.match(/vk.com\/images\/camera_\d{3}\.png/))){
        request.get(link).pipe(res);
    } else {
        console.error('FAIL');
        res.send('FAIL');
    }
});


module.exports = router;
