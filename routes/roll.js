var debug = require('debug')('girl:index.js');
var RND = require('random-js');
var random = new RND(RND.engines.mt19937().autoSeed());
var https = require('https');
var fs = require('fs');
var NodeCache = require('node-cache');
var groupCache = new NodeCache({stdTTL: 60, checkPeriod: 120});



function roll(req, res, ...params){
    //fs.appendFile('./public/log.txt', '[---REQ_END---]\n');
    console.log('[ROLL_CALLED]');
    var addr = '', offset = 0, sex;
    if(params.length) {
        addr = params[0].address;
        offset = random.integer(0, params[0].count);
    } else {
        sex = (req.body.sex == 0) ? random.integer(1,2) : req.body.sex;
        addr = req.body.address;
        addr = addr.match(/^(http|https):\/\/vk\.com\/(\w+)$|^vk\.com\/(\w+)|^\/(\w+)|(\w+)/).slice(2);
        loop:
            for (var e in addr){
                if (addr[e]) {
                    addr = addr[e];
                    break;
                }
            }
    }
    debug('[ADDRESS]:', addr);
    debug('[REQ_SEX]:', sex);

    function askVK(opts){
        var buffer = '', resp;
        return new Promise((resolve, reject) => {

            if (opts.params.offset) {
                opts.params.offset = 0;
                groupCache.get("group", (err, cachedObject) => {
                    if (!err){
                        if (cachedObject == undefined){
                            debug(`[CACHE_MSG]: No cached members count for group with id ${addr}`);
                        } else {
                            debug(`[CACHE_MSG]: Found members count ${cachedObject.count} for ${addr}`);
                            if (cachedObject.count > 1000){
                                opts.params.offset = random.integer(0, cachedObject.count-1000);
                                debug(`[P_OFFSET]: ${opts.params.offset}`);
                            }
                        }
                    } else {
                        debug(`[CACHE_ERR]: ${err}`)
                    }
                });
            }
            var reqPath = '';
            for (var parameter in opts.params){
                if (opts.params.hasOwnProperty(parameter)){
                    reqPath += `${parameter}=${opts.params[parameter]}&`
                }
            }
            reqPath = reqPath.slice(0, -1);
            var reqOptions = {
                host: 'api.vk.com',
                method: 'POST',
                port: 443,
                path: `/method/${opts.method}?${reqPath}`
            };
            debug('[REQ_OPTS]:', reqOptions);
            var rq = https.request(reqOptions, rs => {
                rs.on('data', data => buffer += data);
                rs.on('end', () => {
                    debug(`[BUFFER_DATA]`);
                    buffer = JSON.parse(buffer);
                    if (buffer.response.count){
                        groupCache.set("group", {address: addr, count: buffer.response.count}, (err, success) => {
                            if (!err && success){
                                debug(`[CACHE_MSG]: Added members count = ${buffer.response.count} for ${addr}`);
                            } else  if (err) {
                                debug(`[CACHE_MSG]: Error occured when adding count ${buffer.response.count} for ${addr}`);
                            }
                        });
                    }
                    resp = buffer;
                    //debug(`[RESP_END]: ${resp}`)
                    resolve(resp);
                });

            });
            rq.on('error', err => {
                reject({from: 'askVK', name: '[VK_API_ERR]:', error: err})
            });
            rq.on('socket', socket => {
                socket.setTimeout(1500);
                socket.on('timeout', () => {
                    console.log(`[RESPONSE_END]: !!! R E S P O N S E  E N D !!!`);
                    //rq.abort();
                    socket.end();

                    //return new Error('Network Error: Request timed out.');
                    reject({error: new Error('Network Error: Request timed out.')});
                })

            });
            rq.end();
            //debug(`[BUFFER_1]: ${buffer}`);

        })
    }
    var requestParams_PeopleFromGroup = {
        method: 'groups.getMembers',
        params: {
            group_id: addr,
            fields: 'sex',
            offset: true
        }
    };

    askVK(requestParams_PeopleFromGroup)
        .then(buffer => {
            if (buffer.error){
                console.log(buffer.error);
                res.json(buffer.error);
                Promise.resolve()
            }
            //debug('[BUFFER]:', buffer)
            debug('[RESP_LEN]:', buffer.response.count);
            var peopleArray = [];
            //debug('[VK_RESP]:', buffer);
            if (buffer.response.users.length > 0){
                buffer.response.users.map(e => {
                    if (e.sex == sex && !e.deactivated) peopleArray.push(e);
                });
                debug('[PERSONS_ARRAY_LEN]', peopleArray.length);
            } else throw new Error("No active users");
            var index = random.integer(0, peopleArray.length-1);
            debug('[PERSON_INDEX]:', index);
            return peopleArray[index];
        }, error => {
            res.json(error);
        })
        .then(person => {
            debug('[RANDOM_PERSON]', person.uid);
            var requestParams_GirlProfile = {
                method: 'users.get',
                params: {
                    user_ids: person.uid,
                    fields: 'photo_max_orig'
                }
            };
            return askVK(requestParams_GirlProfile);
        }, error => {
            res.json(error);
        })
        .then(personProfile => {
        debug('[GIRL_PROFILE]', personProfile);
        res.json({randomPerson: personProfile.response[0]});
        return true;
    }, error => {
            res.json(error);
        })
        .catch(error => {
            console.log(error);
            res.end();
            return false;
        });

}


module.exports = roll;
