/**
 * Created by yazun on 18.02.2016.
 */

var https = require('https'),
    debug = require('debug')('girl:new_roll.js'),
    RND = require('random-js'),
    fs = require('fs'),
    random = new RND(RND.engines.mt19937().autoSeed()),
    NodeCache = require('node-cache'),
    groupCache = new NodeCache({stdTTL: 60, checkPeriod: 120});

exports.new_roll = function (req, res){
    debug(`REQ:`, req.query);
    var address = req.query.data.address,
        sex = req.query.data.sex == 0 ? random.integer(1, 2) : req.query.data.sex,
        offset = 0;
        groupCache.get(address, (err, cachedMembersCount) => {
            if (!err){
                if (cachedMembersCount != undefined) {
                    // debug(`[CACHE_MSG]: Found members count ${cachedMembersCount.count} for ${address}`);
                    if (cachedMembersCount > 1000) {
                        offset = random.integer(0, cachedMembersCount - 1000);
                        //debug(`[OFFSET]: ${offset}`);
                    }
                }
            } else {
                debug(`[CACHE_ERR]: ${err}`);
            }
        });
    var response = '';
    debug('[ADDRESS]: ', address);
    debug('[OFFSET]: ', offset);
    var options = {
        hostname: 'api.vk.com',
        port: 443,
        method: 'POST',
        path: `/method/groups.getMembers?group_id=${address}&offset=${offset}&fields=sex`
    };
    //debug('[OPTIONS]: ', options);
    getPersonArray(options, personArray => {
        var randomPerson = randomizePerson(personArray, sex);
        if (randomPerson !== false) {
            getProfile(randomPerson.uid);
        } else {
            sendError(new Error('Profile Error: No Such profile'));
        }
    });

    /**
     *
     * @param  {object} response
     * @param  {int} sex
     * @returns {Object|Boolean} person
     */
    function randomizePerson(response, sex){
        if (response.response.users.length > 0){
            //console.log(response);
            var passedPersons = response.response.users.filter(item => {
                return (item.sex == sex) && (!item.deactivated);
            });
            //console.log(passedPersons);
        }
        return (passedPersons.length) ? passedPersons[random.integer(0, passedPersons.length - 1)] : false;
    }
    /**
     *
     * @param {object} options
     * @param {function} callback
     */
    function getPersonArray(options, callback){
        var accumulator = '';
        var checkGroupRequest = https.request(options, response => {
            response.on('data', data => {
                accumulator += data;
            });
            response.on('end', () => {
                accumulator = JSON.parse(accumulator);
                groupCache.set(address, accumulator.response.count, (err, success) => {
                    if (!err && success){
                        debug(`[CACHE_MSG]: Added members count = ${accumulator.response.count} for ${address}`);
                    } else  if (err) {
                        debug(`[CACHE_MSG]: Error occurred when adding count ${accumulator.response.count} for ${address}`);
                    }
                });
                //console.log(accumulator);
                callback(accumulator);
            })
        });
        checkGroupRequest.on('socket', socket => {
            socket.setTimeout(1000);
            socket.on('timeout', () => {
                checkGroupRequest.abort();
                getPersonArray(options, callback);
                debug('[PROFILE_TIMEOUT]');
            })
        });
        checkGroupRequest.on('error', error => {
            console.error('[GET_PERSON_ARRAY]: ', error);
            //res.json({error: error})
        });
        checkGroupRequest.end();
    }
    /**
     *
     * @param {error} error
     */
    function sendError(error){
        res.json({error: error})
    }
    /**
     *
     * @param {object} personID
     */
    function getProfile(personID){
        debug('[PERSON]: ', personID);
        var buffer = '';
        var profileRequest = https.request({
            hostname: 'api.vk.com',
            port: 443,
            method: 'POST',
            path: `/method/users.get?user_ids=${personID}&fields=photo_max_orig`
        }, response => {
            response.on('data', data => {
                buffer += data;
            });
            response.on('socket', socket => {
                socket.setTimeout(1000);
                socket.on('timeout', () => {
                    debug('[PROFILE_TIMEOUT]');
                    profileRequest.abort();
                    getProfile(personID)
                })
            });
            response.on('end', () => {
                try {
                    buffer = JSON.parse(buffer).response[0];
                    buffer.photo_max_orig = '/photo_proxy?link=' + buffer.photo_max_orig;
                    //buffer.photo_max_orig = buffer.photo_max_orig.match(/camera_400\.jpg$/) ? '/images/no.jpg' : buffer.photo_max_orig;
                    res.json({randomPerson: buffer});
                } catch(e){
                    console.error('[IOException]', e);
                }
                //console.log(`[PROFILE]: ${buffer}`)
            });
        });
        profileRequest.on('socket', socket => {
            socket.setTimeout(1000);
            socket.on('timeout', () => {
                debug('[PROFILE_TIMEOUT]');
                profileRequest.abort();
                getProfile(personID)
            })
        });
        profileRequest.on('error', err => {
            debug('[PROFILE_REQUEST]: ', err);
            //res.json({error: err});
        });
        profileRequest.end();
    }
};