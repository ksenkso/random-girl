var path = require('path');
var webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
module.exports = {
    entry: './public/javascripts/ng',
    output: {
        filename: './public/javascripts/build.js',
        library: 'animation'
    },
    watch: true,
    devtool: (NODE_ENV == 'development') ? 'source-map' : null,
    module: {
        loaders: [{
            test: /\.js$/,
            include: [
                path.resolve(__dirname, 'public/javascripts')
            ],
            loader: 'babel'
        }]
    },
    plugins: []

};

if (NODE_ENV == 'production'){
    module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false,
            drop_console: false,
            unsafe: false
        }
    }))
}
