(function(){
  if (!window.Promise){
     var script = document.createElement('script');
      script.src = '/polyfills/promise.min.js';
      document.body.appendChild(script);
  }
    if (!window.fetch){
        var f = document.createElement('script');
        f.src = '/polyfills/fetch.min.js';
        document.body.appendChild(f);
    }

})();

function headingAnimation(){
    return new Promise((resolve, reject) => {
        var animation = $('.title').animate([
            {transform: 'translateY(0rem)', opacity: 1, offset: 0},
            {transform: 'translateY(-' + getComputedStyle($('.title')).height + ')', opacity: 0, offset: 1}
        ], {
            duration: 500,
            easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
            iterations: 1,
            fill: 'forwards'
        });
        animation.onfinish = resolve(true);
    })
}
// ---

function formAnimation(){
    return new Promise((resolve, reject) => {
        var main = $('.main');
        var resCard = document.createElement('div');
        var img = document.createElement('img');
        img.classList.add('rg-photo');
        //img.src = 'http://cs633328.vk.me/v633328936/82b0/Fo6SM6Remg4.jpg';
        resCard.className = 'rg-card rg-response';
        resCard.appendChild(img);
        main.insertBefore(resCard, main.children[1]);
        resCard.classList.add('open');
        var loader = document.createElement('div');
        loader.className = 'showbox';
        loader.innerHTML = '<div class="loader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>';
        loader.style.opacity = '0';
        resCard.appendChild(loader);
        toggleElementVisibility(loader, false)
            .then(() => resolve(true));
        loader.style.opacity = '1';

        /*var card = $('.rg-card');
        var cardAniamtion = card.animate([
            {transform: 'translateY(0rem)', offset: 0},
            {transform: 'translateY(' + (parseInt(getComputedStyle($('.container')).height)
            - 5*parseInt(getComputedStyle(card).height)) + 'px)',
                offset: 1}
        ], {
            duration: 500,
            iterations: 1,
            easing: 'cubic-bezier(.4, 0, .2, 1)',
            fill: 'forwards'
        });
        cardAniamtion.onfinish = () => {
            card.style.alignSelf = 'flex-end';
            card.classList.add('ready');
            resolve(true);
        };*/
    })
}



document.addEventListener('keypress', e => {
    if (e.keyCode == 13){
        $('button').click();
    }
});



function toggleElementVisibility(element, hide){
    return new Promise((resolve, reject) => {
        var opacityStart = (hide)?1:0;
        var opacityEnd = (hide)?0:1;
        var direction = (hide)?'':'-';
        var animation = element.animate([
            {opacity: opacityStart, transform: 'translateY(0rem)', offset: 0},
            {opacity: opacityEnd, transform: 'translateY(' + direction +'4rem)', offset: 1}
        ], {
            duration: 500,
            easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
            iterations: 1,
            fill: 'forwards'
        });
        animation.onfinish = resolve(true);
    })

}

function $(selector) {
    return document.querySelector(selector);
}
function $$(selector){
    return document.querySelectorAll(selector);
}
$('button').onclick = function (event) {
    var rgResponse = $('.rg-response');
    var photo = $('.rg-photo');
    var name = $('.rg-name');
    var addr = $('input').value;
    var temp = addr.match(/^(http|https):\/\/vk\.com\/(\w+)$|^vk\.com\/(\w+)|^\/(\w+)|(\w+)/);
    addr = temp.slice(2);
    loop:
        for (var e in addr) {
            if (addr[e]) {
                addr = addr[e];
                break loop;
            }
        }
    function checkFirstUse(){
        return (!!~$('.rg-form').className.indexOf('ready')) ? Promise.all([photo, name].map(el => toggleElementVisibility(el.parentElement, true))) : Promise.resolve(true);
    }

    checkFirstUse().then((firstUse) => {
        if (firstUse === true){
            firstUse = false;
            headingAnimation()
            .then(firstUse => {
                var list = $$('li');
                list = Array.prototype.slice.call(list, 0).reverse();
                var timers = [0, 200, 400];

                function animate(pics, func){
                    return Promise.all(pics.map(func))
                }
                function animatePic(pic, i){
                    return new Promise((resolve, reject) => {
                        setTimeout(() => {
                            var player = toggleElementVisibility(pic, true);
                            resolve();
                        }, timers[i]);
                    })
                }
                animate(list, animatePic)
                    .then(() => $('ul').style.display = 'none')
                    .then(formAnimation)
            });
        } else {
            console.log('TEST')
        }
    })
    .then(() => {
        fetch('/random', {method: 'POST', headers: {'Content-Type': 'application/x-www-form-urlencoded'}, body: `address=${addr || window.location.pathname.substring(1)}`})
            .then(data => {
                console.log('SERVER RESPONDED:', data.headers);
                window.history.pushState('', '', data.headers.get('Location'));
                return data.json();
            })
            .then(response => {
                console.log('RESPONSE PARSED:', response);
                var randomGirl = response.randomGirl;
                var photoLink = $('.rg-link-photo') || document.createElement('a');
                var nameLink = $('.rg-link-name') || document.createElement('a');

                if (!rgResponse){
                    rgResponse = document.createElement('div');
                    rgResponse.classList.add('rg-response');

                    photo = document.createElement('img');
                    photo.classList.add('rg-photo');

                    photoLink.className = 'rg-link rg-link-photo';
                    photoLink.appendChild(photo);
                    photoLink.target = '_blank';
                    photoLink.appendChild(photo);

                    name = document.createElement('h1');
                    name.classList.add('rg-name');

                    nameLink.className = 'rg-link rg-link-name';
                    nameLink.target = '_blank';
                    nameLink.appendChild(name);
                    rgResponse.appendChild(nameLink);
                    rgResponse.appendChild(photoLink);
                    $('.main').appendChild(rgResponse);
                }

                photo.src = randomGirl.photo_max_orig;
                nameLink.href = `http://vk.com/id${randomGirl.uid}`;
                name.innerText = `${randomGirl.first_name} ${randomGirl.last_name}`;
                photoLink.href = `http://vk.com/id${randomGirl.uid}`;
                toggleElementVisibility(nameLink, false);
                toggleElementVisibility(photoLink, false)
                    .then(() => $('button').scrollIntoView());
            })
    })
};
