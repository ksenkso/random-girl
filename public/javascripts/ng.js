
var app = angular.module('randomPerson', ['ngMaterial']);
app.config(function($mdThemingProvider, $compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|ftp|blob):|data:image\//);
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|ftp|blob):|data:image\//);
    $mdThemingProvider.theme('default')
    .primaryPalette('green')
    .accentPalette('blue');
});
app.controller('rgCtrl', function($scope, $http){
    $scope.callRender = function(e){
        if (e.keyCode == 13){
            $scope.renderContent();
        }
    };
    window.$scope = $scope;
    $scope.firstUse = true;
    $scope.Person = {};
    $scope.loading = false;

    /**
     *
     * @returns {Boolean| Error}
     */
    $scope.checkInput = () => {
        console.log('CHECK_INPUT');
        var radio = $('[aria-checked=true]').attr('value');
        if (radio){
            $scope.sex = radio;
        }
        var address = $('#address')[0].value;
        var r1 = /^(http|https):\/\/vk\.com\/club(\d+)/,
            r2 = /^(http|https):\/\/vk\.com\/([a-zA-Z_0-9]+)/,
            r3 = /^(vk\.com)\/club(\d+)/,
            r4 = /^(vk\.com)\/([a-zA-Z_0-9]+)/,
            r5 = /club(\d+)/,
            r6 = /([a-zA-Z_0-9]+)/;
        address = address.match(r1) || address.match(r2) || address.match(r3) || address.match(r4) || address.match(r5) || address.match(r6);

        $scope.address = address ? address[address.length - 1] : false;
        return $scope.sex && $scope.address ? Promise.resolve(true) : Promise.reject(new Error('Input Error: you must choose sex and group address.'));
    };
    /**
     *
     * @returns {Promise}
     */
    $scope.fetch = () => {
        console.log($scope.firstUse);


        return $scope.checkInput().then(() => {

            $('ul').fadeOut(500);
            if ($scope.firstUse){
                $scope.firstUse = false;
                $('.loader_wrapper').animate({
                    height: '400px',
                    width: '400px'
                }, () => {
                    $('md-progress-circular').slideDown();
                });
            } else {
                $('.loader_wrapper .photo_image').css({
                    'filter': 'blur(7px)',
                    'webkitFilter': 'blur(7px)',
                    'oFilter': 'blur(7px)',
                    'msFilter': 'blur(7px)',
                    'mozFilter': 'blur(7px)'
                });
                $('md-progress-circular').slideDown();
            }
            console.log('FETCH');
            return fetch('/rnd', {
                method: 'GET',
                headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Origin': `${window.location.protocol}//${window.location.host}:${window.location.port}`},
                body: `address=${$scope.address}&sex=${$scope.sex}`,
                mode: 'cors'
            })
        });

    };
    /**
     *
     * @returns {Promise}
     */
    $scope.loadContent = () => {
        return $scope.fetch().then(response => {
            console.log('JSON', response);
            window.resp = response;
            return response.json();
        }).then(response => {
            console.log('LOAD_PROFILE');
            console.log(response);
            if (response.randomPerson){
                $scope.randomPerson = response.randomPerson;
                window.$scope = $scope;
            } else if (response.error){
                console.error(response.error);
                Promise.reject(new Error('Response Error:' + response.error))
            }
            return $scope.randomPerson;
        })
    };
    /**
     *
     * This function initiates a chain of promises that are defined up here.
     * After the promise chain returns an object that contains information about
     * person or error that has been occurred in the promise chain.
     */
    $scope.renderContent = () => {
        $scope.loadContent().then(person => {
            console.log('[PERSON]:' ,person);

            $http({
                method: 'GET',
                url: person.photo_max_orig,
                responseType: 'arraybuffer'
            }).then(response => {
                $('md-progress-circular').fadeOut();
                $scope.personLink = `https://vk.com/id${person.uid}`;
                $scope.personName = `${person.first_name} ${person.last_name}`;
                try {

                    var byteArray = new Uint8Array(response.data),
                        blob = new Blob([byteArray], {'type': 'image\/jpeg'});
                    $scope.personPhoto = URL.createObjectURL(blob);
                    window.tempImage = new Image();
                    window.tempImage.onload = function(){
                        $('.loader_wrapper').animate({
                            height: window.tempImage.naturalHeight,
                            width: window.tempImage.naturalWidth
                        }, () => {
                            $('.photo_image').css({
                                'background-image': `url(${tempImage.src})`,
                                'filter': 'blur(0px)',
                                'webkitFilter': 'blur(0px)',
                                'oFilter': 'blur(0px)',
                                'msFilter': 'blur(0px)',
                                'mozFilter': 'blur(0px)'
                            })
                        });
                    };
                    window.tempImage.src = URL.createObjectURL(blob);

                } catch(e){
                    console.error(e);
                    console.error(new Error('ArrayBuffer Error'));
                }
                window.photolink = response.data;
                $scope.loading = false;
                $scope.loaded = true;
            }, err => {
                window.respErr = err;
                console.error('resp', err);
                console.log(err.stack)
            });
        }).catch(err => {
            if (err) {
                console.error(err)
            }
        })
    }
});
