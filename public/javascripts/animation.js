


'use strict';
import {State, CustomAnimation, Loader, MaterialElement, RgCard, RgLink, RgPhoto, $} from './classes.js';
$('button').onclick = () => {

     State.responseCard = State.responseCard || new RgCard(document.createElement('div'), 'response');
    (function () {
        return new Promise((resolve, reject) => {
            if (!State.person.name){
                var header = $('.main .title');
                var opts = new CustomAnimation('down', '0', false);
                var headerAnimation = header.animate(opts[0], opts[1]);
                headerAnimation.onfinish = () => {
                    State.responseCard.spawn($('.main'), 1).then(() => resolve());
                }
            } else {
                resolve();
            }

        })
    })()
    .then(() => {
        var address = $('input').value;
        var temp = address.match(/^(http|https):\/\/vk\.com\/(\w+)$|^vk\.com\/(\w+)|^\/(\w+)|(\w+)/);
        address = temp.slice(2);
        loop:
            for (var e in address) {
                if (address[e]) {
                    address = address[e];
                    break loop;
                }
            }
        State.groupAddress = address;
        State.responseCard.showLoader();
        return fetch('/random', {
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            body: `address=${State.groupAddress || window.location.pathname.substring(1)}`
        })
    })
    .then(data => {

        return data.json();
    }).then(response => {
        if (response.randomGirl){
            var rg  = response.randomGirl;
            State.person.name = `${rg.first_name} ${rg.last_name}`;
            State.person.photo = rg.photo_max_orig;
            State.person.id = rg.uid;

        } else if (response.error){
             Promise.reject(response.error);
        }
    }).then(() => {
        State.responseCard.hideLoader();
        return State.responseCard.showPhoto(State.person.photo, State.person.id)
    }).then(() => {
        State.responseCard.expand();
        State.responseCard.showHeader(State.person.name);
        State.firstUse = false;
    }).catch(err => {
        console.log(err);

        State.responseCard.showHeader(err.msg);
    })
};