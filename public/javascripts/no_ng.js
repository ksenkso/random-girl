/**
 * Created by yazun on 26.02.2016.
 */
var $scope = {},
    sex,
    firstUse = true,
    person,
    address;
/**
 *
 * @param {MouseEvent} e
 */
function startRoll(e){
    if (e.keyCode == 13){
        roll();
    }
}
function roll(){
    if (checkInput()){
        getRandomProfile()
            .done(response => {
                renderPerson(response.randomPerson);
            })
            .error(error => {
                console.error(error);
            })
    }
}

/**
 *
 * @returns {Boolean}
 */
function checkInput() {
    var radio = $('[aria-checked=true]').attr('value'),
        address = $('#address')[0].value,
        r1 = /^(http|https):\/\/vk\.com\/club(\d+)/,
        r2 = /^(http|https):\/\/vk\.com\/([a-zA-Z_0-9]+)/,
        r3 = /^(vk\.com)\/club(\d+)/,
        r4 = /^(vk\.com)\/([a-zA-Z_0-9]+)/,
        r5 = /club(\d+)/,
        r6 = /([a-zA-Z_0-9]+)/;
    sex = radio ? radio : '0';
    address = address.match(r1) || address.match(r2) || address.match(r3) || address.match(r4) || address.match(r5) || address.match(r6);
    $scope.address = Array.isArray(address) ? address[address.length - 1] : false;
    return sex && address ? true : false;
}

/**
 *
 * @returns {jQueryAjaxObject}
 */
function getRandomProfile(){
    return $.ajax({
        beforeSend: () => {
            $('ul').fadeOut(500);
            if (firstUse){
                firstUse = false;
                $('.loader_wrapper').animate({
                    height: '400px',
                    width: '400px'
                }, () => {
                    $('md-progress-circular').slideDown();
                });
            } else {
                $('.loader_wrapper .photo_image').css({
                    'filter': 'blur(7px)',
                    'webkitFilter': 'blur(7px)',
                    'oFilter': 'blur(7px)',
                    'msFilter': 'blur(7px)',
                    'mozFilter': 'blur(7px)'
                });
                $('md-progress-circular').slideDown();
            }
        },
        url: '/rnd',
        type: 'GET',
        dataType: 'json',
        data: {address: address, sex: sex},
        headers: {
            'Origin': `${window.location.protocol}//${window.location.host}:${window.location.port}`
        }
    });

}

function animatePhoto(photo){
    $('.loader_wrapper').animate({
        height: photo.naturalHeight,
        width: photo.naturalWidth
    }, () => {
        $('.photo_image').css({
            'background-image': `url(${photo.src})`,
            'filter': 'blur(0px)',
            'webkitFilter': 'blur(0px)',
            'oFilter': 'blur(0px)',
            'msFilter': 'blur(0px)',
            'mozFilter': 'blur(0px)'
        })
    });
}

/**
 *
 * This function initiates a chain of promises that are defined up here.
 * After the promise chain returns an object that contains information about
 * person or error that has been occurred in the promise chain.
 */
function renderPerson(person){
    $.get(person.photo_max_orig)
        .done(() => {
            var tempImage = new Image();
            tempImage.src = person.photo_max_orig;
            animatePhoto(tempImage)
        })
        .error(() => {
            animatePhoto({
                naturalHeight: "400px",
                naturalWidth: "400px",
                src: "/images/min/no_photo.jpg"
            });
        })
}
$scope.renderContent = () => {
    $scope.loadContent().then(person => {
        console.log('[PERSON]:' ,person);

        $http({
            method: 'GET',
            url: person.photo_max_orig,
            responseType: 'arraybuffer'
        }).then(response => {
            $('md-progress-circular').fadeOut();
            $scope.personLink = `https://vk.com/id${person.uid}`;
            $scope.personName = `${person.first_name} ${person.last_name}`;
            try {

                var byteArray = new Uint8Array(response.data),
                    blob = new Blob([byteArray], {'type': 'image\/jpeg'});
                $scope.personPhoto = URL.createObjectURL(blob);
                window.tempImage = new Image();
                window.tempImage.onload = function(){
                    $('.loader_wrapper').animate({
                        height: window.tempImage.naturalHeight,
                        width: window.tempImage.naturalWidth
                    }, () => {
                        $('.photo_image').css({
                            'background-image': `url(${tempImage.src})`,
                            'filter': 'blur(0px)',
                            'webkitFilter': 'blur(0px)',
                            'oFilter': 'blur(0px)',
                            'msFilter': 'blur(0px)',
                            'mozFilter': 'blur(0px)'
                        })
                    });
                };
                window.tempImage.src = URL.createObjectURL(blob);

            } catch(e){
                console.error(e);
                console.error(new Error('ArrayBuffer Error'));
            }
            window.photolink = response.data;
            $scope.loading = false;
            $scope.loaded = true;
        }, err => {
            window.respErr = err;
            console.error('resp', err);
            console.log(err.stack)
        });
    }).catch(err => {
        if (err) {
            console.error(err)
        }
    })
}