# Changelog v0.3.0
Added:

 - Application was moved to AngularJS and jQuery; 
 - Proxy for all photos: now photo of a person streams from application server that fixes issue with wrong response headers from VK API;
 - When new photo is loading, old photo gets some blur.
- - -
Fixed:

 - Cache bug with wrong offset number was fixed;
 - Now circular loader work correctly;
 - All animations.
 
